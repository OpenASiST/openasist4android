# OpenASiST

Die OpenASiST-App unterstützt Studenten in ihrem Studienalltag und wird zu einem unersetzlichen Begleiter.
Vom Design bis zum Funktionsumfang kann die OpenASiST-App an jede Universität oder Institut angepasst werden. Weithin soll dieses Projekt eine Vorlage bieten, um einen einfachen Einstieg in die Individualisierung dieser App zu ermöglichen.

## Einstieg

Diese Anleitung möchte einen einfachen Einstieg ermöglichen und erläutert, wie die App kompiliert und paketiert wird. Nach Abschluss der Anleitung sollte eine Projektkopie vorhanden sein, die für Entwicklungszwecke verwendet werden kann.

### Voraussetzungen

Um die folgenden Anweisungen durchführen zu können, sollten grundlegende Kenntnisse über git vorhanden sein. Weiterhin wird ein installiertes Android-Studio benötigt, wohingegen Gradle sich mit Hilfe des beigelegten Gradle-Wrappers automatisch installiert.

### Repository klonen

Bitte erstellen Sie eine Arbeitskopie des Projektes mit folgendem Befehl:
```
git clone --recursive git@gitlab.hrz.tu-chemnitz.de:OpenASiST/openasist4android.git
```

Der '--recursive'-Parameter wird dazu verwendet, um alle im Repository registrierten Submodule mit auszuchecken.

Sie haben das Repository ohne den '--recursive'-Parameter geklont? Dann wenden Sie folgende Befehlsfolge an:
```
git submodule init
git submodule update
```

Somit werden die Submodule nachträglich initialisiert und eingebunden.


### Projekt in Android-Studio öffnen

Android-Studio erlaubt einen leichten Zugang zur Android-Programmierung, weshalb dieser Ansatz empfohlen wird.

Starten Sie Ihr Android-Studio und öffnen Sie den Projektordner. Sie werden gefragt, ob Sie die Gradle-Version aktualisieren möchten. Bitte verweigern Sie den Aktualisierungsvorgang, um die Kompilierbarkeit Ihrer Projektkopie zu erhalten. Android Studio wird Ihnen alle benötigten Abhängigkeiten auflisten, welche über den SDK-Manager installierbar sind. Der SDK-Manager ist in der Menüleiste unter Tools zu finden. Die App ist nun kompilierbar.

### Projekt mit Kommandokonsole kompilieren
Laden Sie die das Android-SDK von der offiziellen Seite herunter und entpacken Sie dieses an einem beliebigen Ort. Legen Sie eine Datei namens local.properties in dem geklonten Projektverzeichnis an und fügen Sie den Speicherort des Android-SDK wie folgt ein:
```
sdk.dir=/home/user/Android/SDK
```
Der Datenpfad ist an Ihre Installation anzupassen.
Installieren Sie nun die benötigten Abhängigkeiten im Android-SDK, dafür können Sie den im Android-SDK mitgelieferten SDK-Manager verwenden.
Bitte verwenden Sie nicht die systemweit installierte Gradle-Instanz, um den Build-Prozess auszuführen. Stattdessen sollte der mitgelieferte Gradle-Wrapper verwendet werden. Führen Sie dazu folgenden Befehl im Projektordner aus:
```
./gradlew OpenASiST:assembleDebug
```

Dieser Wrapper installiert die benötigte Gradle-Version und startet den Kompiliervorgang. Weiterhin wird er Sie auf fehlende Abhängigkeiten aufmerksam machen. 

## Mitwirken

Bitte lesen Sie die [CONTRIBUTING.md](https://gitlab.hrz.tu-chemnitz.de/OpenASiST/openasist4android_core/blob/develop/CONTRIBUTING.md) bevor Sie Ihre Änderungen veröffentlichen. Für die Einreichung Ihrer Änderungen sollten Sie den Gitflow Workflow anwenden. Dadurch können wir Ihre Änderungen leichter und schneller einpflegen.


## Lizenz

Dieses Projekt verwendet die Apache-Lizenz Version 2.0. Lesen Sie die [LICENSE](LICENSE) für weitere Informationen.
